      <div class="row">
          <div class="large-12 columns">
              <hr />
          </div>
      </div>

      <!-- footer -->
      <div id="footer" class="row">
          <div class="large-12 columns text-center">
            <p>&copy; Small Giants <? echo date('Y'); ?>  |  <a href="mailto:info@smallgiantsonline.com">info@smallgiantsonline.com</a></p>
            <p><strong>Phoenix</strong>      P: 602.314.5549     <a href="https://www.google.com/maps/place/4531+N+16th+St+%23126,+Phoenix,+AZ+85016/@33.5032935,-112.0471579,17z/data=!3m1!4b1!4m2!3m1!1s0x872b0d62a14bbc03:0xed274338a77b218" target="_blank">4531 N. 16th St., Suite 126, Phoenix, AZ 85016</a></p>
            <p><strong>Denver</strong>      P: 720.515.3477      <a href="https://www.google.com/maps/place/770+W+Hampden+Ave+%23340,+Englewood,+CO+80110/@39.652934,-104.996669,17z/data=!3m1!4b1!4m2!3m1!1s0x876c8079dae0b827:0x1394019a1fd739da" target="_blank">4610 South Ulster Street, Suite 150, Denver, Colorado 80237</a></p>
          </div>
      </div>

    </div>

    <?php wp_footer(); ?>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-10399395-5', 'auto');
      ga('send', 'pageview');

    </script>
  </body>
</html>
