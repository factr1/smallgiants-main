<?php
  /* Template Name: Contact Template */
  get_header();
?>

  <!-- primary -->
  <div id="primary-cont" class="row">
      <div class="large-8 large-push-4 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel0">
                  <?
                    if(have_rows('page-sections')) {
                        while (have_rows('page-sections')) { the_row();
                            if(get_row_layout() == 'section-image') {
                                $imageArray = wp_get_attachment_image_src(get_sub_field('page-image', get_the_ID()), 'office-page');
                                ?>
                                <img src="<? echo $imageArray[0]; ?>">
                                <?
                            }
                        }
                    }
                  ?>
              </div>
              <div class="content" id="panel1">
                  <div class="row">
                      <div class="large-12 columns">
                          <h4><? echo get_the_title(29); ?></h4>
                          <br />
                          <?
                            if( have_rows('speaking-engagements', 29) ) {
                                while ( have_rows('speaking-engagements', 29) ) { the_row();
                                    echo '<h5>'.get_sub_field('topic').'</h5>';
                                    echo '<span class="meta" style="display:block;font-size:.9em;margin-bottom:10px;"><span class="date">'.get_sub_field('date').'</span> / '.get_sub_field('location').'</span>';
                                    echo '<p>'.get_sub_field('description').' ';
                                    if (get_sub_field('reg-link') != '') {
                                        echo '<a href="'.get_sub_field('reg-link').'" target="_blank">Register Now</a></p>';
                                    } else {
                                        echo '</p>';
                                    }

                                }
                            }
                          ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <h5>Headquarters:</h5>
          <span>PHONE:</span>
          <span>602.314.5549</span><br />
          <span>ADDRESS:</span><br />
          <span>4531 N. 16th St., Suite 126,<br /> Phoenix, AZ 85016</span><br />

          <h5>Denver, CO:</h5>
          <span>PHONE:</span>
          <span>720.515.3477</span><br />
          <span>ADDRESS:</span><br />
          <span>770 W. Hampden Ave., Suite 340,<br /> Englewood, CO 80110</span>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab>
                      <dd><a class="dropshadow-extra-light" href="#" onClick="window.location.href='<? echo esc_url( home_url( '/' ) ); ?>speaking-engagements/'"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Speaking Engagements</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="mailing-list" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-3.png"> Join Our Mailing List</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <p>Please complete the form below to be immediately directed to a downloadable interactive PDF of our services and work.</p>

      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>

      <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="mailing-list" class="reveal-modal" data-reveal>
      <h5>Give us a buzz</h5>

      <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>

      <a class="close-reveal-modal">&#215;</a>
  </div>
  <br />
  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns text-center">
          <img src="<? bloginfo('template_url'); ?>/assets/img/tgg-icon.png">
      </div>
      <div class="large-8 medium-8 columns">
          <a href="#" data-reveal-id="mailing-list" class="button button-buzz dropshadow-extra-light"><img src="<?php bloginfo('template_url');?>/assets/img/email.png"> Give us a buzz</a>
          <h2 class="blue">WE’D LOVE TO HEAR FROM YOU!</h2>
      </div>
  </div>

<?php get_footer(); ?>
