<?php get_header(); ?>

  <!-- primary -->
  <div id="primary-cont" class="row">
      <div class="large-8 large-push-4 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel0" style="padding:20px 25px;">
                  <h4 style="margin-bottom:20px;"><? echo get_the_title(29); ?></h4>

                  <?
                    if( have_rows('speaking-engagements', 29) ) {
                        while ( have_rows('speaking-engagements', 29) ) { the_row();
                            echo '<h5>'.get_sub_field('topic').'</h5>';
                            echo '<span class="meta" style="display:block;font-size:.9em;margin-bottom:10px;"><span class="date">'.get_sub_field('date').'</span> / '.get_sub_field('location').'</span>';
                            echo '<p>'.get_sub_field('description').' ';
                            if (get_sub_field('reg-link') != '') {
                                echo '<a href="'.get_sub_field('reg-link').'" target="_blank">Register Now</a></p>';
                            } else {
                                echo '</p>';
                            }

                        }
                    }
                  ?>
              </div>
              <div class="content" id="panel1">
                  <div class="row">
                      <div class="large-12 columns">
                          <h4><? echo get_the_title(29); ?></h4>
                          <br />
                          <?
                            if( have_rows('speaking-engagements', 29) ) {
                                while ( have_rows('speaking-engagements', 29) ) { the_row();
                                    echo '<h5>'.get_sub_field('topic').'</h5>';
                                    echo '<span class="meta" style="display:block;font-size:.9em;margin-bottom:10px;"><span class="date">'.get_sub_field('date').'</span> / '.get_sub_field('location').'</span>';
                                    echo '<p>'.get_sub_field('description').' ';
                                    if (get_sub_field('reg-link') != '') {
                                        echo '<a href="'.get_sub_field('reg-link').'" target="_blank">Register Now</a></p>';
                                    } else {
                                        echo '</p>';
                                    }

                                }
                            }
                          ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <h5>Speakers:</h5>
          <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
              <option value="#">Select a Speaker</option>
          <?
              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'team', 'post__in' => array(151, 152, 156));
              $parent = new WP_Query( $args );
              if ($parent->have_posts()) {
                  while ( $parent->have_posts() ) {
                      $parent->the_post();
          ?>
                      <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
          <?
                  }
              }
              wp_reset_query();
          ?>
          </select>
          <br />

          <h5>Topics Offered:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                      <option value="#">Select a Topic</option>
                      <?
                          $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'topics');
                          $parent = new WP_Query( $args );
                          if ($parent->have_posts()) {
                              while ( $parent->have_posts() ) {
                                  $parent->the_post();
                      ?>
                                  <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                      <?
                              }
                          }
                          wp_reset_query();
                      ?>
                  </select>
              </div>
          </div>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                      <dd><a class="dropshadow-extra-light" href="#panel1"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-1.png"> Why Small Giants</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="book-speaker" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Book A Speaker/Training</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="mailing-list" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-3.png"> Join our mailing list</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="mailing-list" class="reveal-modal" data-reveal>
      <h5>Give Us a Buzz</h5>
      <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="book-speaker" class="reveal-modal" data-reveal>
      <h5>Book a Speaker</h5>
      <?php echo do_shortcode('[gravityform id="3" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns text-center">
          <img src="<? bloginfo('template_url'); ?>/assets/img/tgg-icon.png">
      </div>
      <div class="large-8 medium-8 columns">
          <a href="#" data-reveal-id="mailing-list" class="small button dropshadow-extra-light">Give us a buzz</a>
          <h2 class="blue">WE’D LOVE TO HEAR FROM YOU!</h2>
      </div>
  </div>

<?php get_footer(); ?>
