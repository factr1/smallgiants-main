<?php get_header(); ?>

  <!-- primary -->
  <div id="primary-cont" class="row">
      <div class="large-4 columns sidebar dropshadow">
          <h5>Client Type:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="#">Select Client Type</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'client-types');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Services:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select Service</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Project Type:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select Project Type</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'project-types');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab>
                      <dd><a class="dropshadow-extra-light" href="#panel2"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Speaking Engagements</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      </div>
      <div class="large-8 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel1">
                  <div class="row">
                      <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'projects',
                                      'meta_query'	=> array(
                                    		'relation'		=> 'AND',
                                    		array(
                                    			'key'		=> 'project-type',
                                    			'value'		=> get_the_ID(),
                                    			'compare'	=> 'LIKE'
                                    		)
                                    	)
                                    );
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                                <? if (have_rows('project-slide')) { ?>
                                <ul class="bxslider">
                                <? while ( have_rows('project-slide')) { the_row(); ?>
                                    <li><? the_sub_field('image'); ?></li>
                                <? } ?>
                                </ul>
                                <? } ?>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>

                      <div class="large-4 columns">
                          <h4>Client:</h4>
                          <p>Commercial Real Estate</p>

                          <h4>Who:</h4>
                          <p>Commercial Real Estate</p>
                      </div>
                      <div class="large-8 columns">
                          <ul class="bxslider">
                              <li><img src="http://placehold.it/645x470"></li>
                              <li><img src="http://placehold.it/645x470/f5831f/ffffff"></li>
                              <li><img src="http://placehold.it/645x470/ff0000/ffffff"></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="content" id="panel2">
                  <div class="row">
                      <div class="large-12 columns">
                          <h4><? echo get_the_title(29); ?></h4>
                          <? echo get_field('page-content-lr', 29); ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns">
          <div class="panel buzz">
          	<h5>Latest Buzz:</h5>
          	<?
              query_posts('order=DESC&orderby=date&posts_per_page=4&cat=-9,-10');
              if ( have_posts() ) {
            ?>
              <ul>
            <?
                  while ( have_posts() ) { the_post();
                      $postTitle = substr($careerTitle, 0, 120);
            ?>
                    <li><a href="<?php the_permalink() ?>"><span class="title"><? echo get_the_title(); ?></span></a></li>
            <?    } ?>
              </ul>
            <?
              }
              wp_reset_query();
            ?>
          </div>
      </div>
      <div style="">
      <?
          $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services', 'posts_per_page' => '4');
          $parent = new WP_Query( $args );
          if ($parent->have_posts()) {
              while ( $parent->have_posts() ) {
                  $parent->the_post();

                  if (get_the_title() == 'Graphic Design') {
                      $serviceTitle = 'Graphic<br />Design';
                  } else {
                      $serviceTitle = get_the_title();
                  }
      ?>

                  <div class="circle dropshadow-light <? echo get_field('temp_color', get_the_ID()); ?>"><a href="<? echo get_permalink(get_the_ID()); ?>"><span><? echo $serviceTitle; ?></span></a></div>
      <?
              }
          }
          wp_reset_query();
      ?>
      </div>
  </div>

<?php get_footer(); ?>
