<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html class="no-js" lang="en">
  <head>
      <noscript><img src="http://www.sas15k01.com/49133.png" style="display:none;" /></noscript>
      <meta charset="<? bloginfo( 'charset' ); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <title><?php echo get_bloginfo('name'); ?></title>

      <meta name="description" content="<? echo get_bloginfo('description'); ?>">
      <meta name="keywords" content="<? echo get_field('keywords', 5); ?>">
      <meta name="author" content="">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />

      <!--[if IE 7]><link rel="stylesheet" href="<? bloginfo('template_url'); ?>/assets/css/fontello-ie7.css"><![endif]-->

      <link href="<? echo esc_url( home_url( '/' ) ); ?>humans.txt" rel="author">
      <link href="<? bloginfo('template_url'); ?>/assets/icons/favicon.ico" rel="shortcut icon" type="image/x-icon">

      <!-- SG Icons -->
      <link rel="icon" type="image/png" href="<? bloginfo('template_url'); ?>/assets/icons/sg-16x16.ico">
      <link rel="apple-touch-icon" type="image/png" href="<? bloginfo('template_url'); ?>/assets/icons/sg-144x144.png">

      <?php wp_head(); ?>
  </head>
  <body <?php body_class();?>>

  <div class="fullWidth">
      <div id="bg-logo"></div>
      <!-- header -->
      <div class="row fullWidth">
          <div id="header" class="row">
              <div class="large-12 large-centered columns" style="max-width:60em;margin:0 auto;">

                  <div class="contain-to-grid header-section show-for-small-only" style="z-index:1000000000000000;">
                      <nav class="top-bar" data-topbar role="navigation" data-options="mobile_show_parent_link: false,scrolltop: true, custom_back_text: true, back_text: ''">
                          <!-- TITLE AREA & LOGO -->
                            <ul class="title-area">
                                <li class="name">
                                </li>
                                <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                            </ul> <!-- END TITLE AREA & LOGO -->

                            <!-- MENU ITEMS -->
                            <section class="top-bar-section">
                                <ul class="right">
                                    <li>
                                        <a href="<?php bloginfo('url');?>/">Home</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url');?>/experience/">Experience</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url');?>/team/">Team</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url');?>/contact/">Contact</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url');?>/buzz/">Buzz</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url');?>/recruiting-2/">Recruiting</a>
                                    </li>
                                    <li>
                                        <a href="<?php bloginfo('url');?>/services/photo/">Photo</a>
                                    </li>
                                </ul>
                            </section> <!-- END MENU ITEMS -->
                      </nav>
                  </div>

                  <div class="large-4 medium-4 small-12 columns small-only-text-center text-right">
                      <a href="<? echo esc_url( home_url( '/' ) ); ?>"><img src="<? bloginfo('template_url'); ?>/assets/img/sm-logo.png" alt="Small Giants" title="Small Giants" width="207" height="83"></a>
                  </div>
                  <div class="large-5 medium-5 small-12 columns hide-for-small-only" style="padding:0;">
                      <?php wp_nav_menu( array('menu' => 'Main Navigation' )); ?>
                  </div>
                  <div class="large-3 medium-3 small-12 columns text-right hide-for-small-only" style="padding:0;">
                      <a style="display:inline-block;" href="<? echo esc_url( home_url( '/' ) ); ?>services/photo"><img src="<? bloginfo('template_url'); ?>/assets/img/photo.png" width="100" height="70" alt="recruiting" title="recruiting"></a>
                      <a style="display:inline-block;" href="<? echo esc_url( home_url( '/' ) ); ?>recruiting-2"><img src="<? bloginfo('template_url'); ?>/assets/img/recruit.png" width="100" height="70" alt="recruiting" title="recruiting"></a>
                  </div>
                  <div class="large-12 medium-12 small-12 columns text-right hide-for-small-only" id="social">
                      <a href="https://www.facebook.com/smallgiants" target="_blank"><img src="<? bloginfo('template_url'); ?>/assets/img/social/facebook.png" width="15" height="15"></a>
                      <a href="https://twitter.com/SmallGiants" target="_blank"><img src="<? bloginfo('template_url'); ?>/assets/img/social/twitter.png" width="15" height="15"></a>
                      <a href="https://www.linkedin.com/company/small-giants" target="_blank"><img src="<? bloginfo('template_url'); ?>/assets/img/social/linkedin.png" width="15" height="15"></a>
                      <a href="https://plus.google.com/115029985959971795055/posts" target="_blank"><img src="<? bloginfo('template_url'); ?>/assets/img/social/google_plus.png" width="15" height="15"></a>
                      <a href="https://instagram.com/small_giants/" target="_blank"><img src="<? bloginfo('template_url'); ?>/assets/img/social/instagram.png" width="15" height="15"></a>
                      <a href="https://www.pinterest.com/smallgiants/" target="_blank"><img src="<? bloginfo('template_url'); ?>/assets/img/social/pinterest.png" width="15" height="15"></a>
                  </div>


              </div>
          </div>
      </div>
