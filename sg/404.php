<?php
  /* Template Name: Home Page */
  get_header();
?>

      <!-- primary -->
      <div id="primary-cont" class="row">
          <div class="large-4 columns sidebar dropshadow">
              <h5>Our Mission:</h5>
              <p>Building progressive marketing solutions for the design, construction and commercial real estate industry through unparalleled experience and passion.</p>
              <div class="bug" style="position:relative;float:right;">
                  <?
                      $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '289');
                      $parent = new WP_Query( $args );
                      if ($parent->have_posts()) {
                          while ( $parent->have_posts() ) {
                              $parent->the_post();
                              $bugFact = htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8'));
                          }
                      }
                      wp_reset_query();
                  ?>
                  <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/bug-4.png"  onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 200});">
              </div>
              <div class="row">
                  <div class="small-11 small-centered columns">
                      <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                          <dd><a class="dropshadow-extra-light" href="#panel1"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-1.png"> Why Small Giants</a></dd>
                          <dd><a class="dropshadow-extra-light" href="#" onClick="window.location.href='<? echo esc_url( home_url( '/' ) ); ?>speaking-engagements/'"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Speaking Engagements</a></dd>
                          <dd><a class="dropshadow-extra-light" data-reveal-id="mailing-list" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-3.png"> Join our mailing list</a></dd>
                          <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                      </dl>
                  </div>
              </div>
          </div>
          <div class="large-8 columns stage">
              <div class="tabs-content">
                  <div class="content active" id="panel0">
                       <h1>Error (404)</h1>
                       <p>
                         Oops! The page you were looking for appears to have been
                         moved, deleted or does not exist. Please use the navigation
                         above to find what you’re looking for or head straight
                         to our home page.
                       </p>
                  </div>
                  <div class="content" id="panel1">
                      <div class="row">
                          <div class="large-5 columns">
                              <h4><? echo get_the_title(27); ?></h4>
                              <? echo get_field('page-content-lr', 27); ?>
                          </div>
                          <div class="large-7 columns">
                              <?
                                if(have_rows('page-sections', 27)) {
                                    while (have_rows('page-sections', 27)) { the_row();
                                        if(get_row_layout() == 'section-image'):
                                            $imageArray = wp_get_attachment_image_src(get_sub_field('page-image', 27), 'services');
                                            ?>
                                            <img src="<? echo $imageArray[0]; ?>" style="border:4px solid #ccc;">
                                            <?

                                        endif;
                                    }
                                }
                              ?>
                          </div>
                      </div>
                  </div>
                  <div class="content" id="speaking-engagements">

                      <h4><? echo get_the_title(29); ?></h4>
                      <br />
                      <?
                        if( have_rows('speaking-engagements', 29) ) {
                            while ( have_rows('speaking-engagements', 29) ) { the_row();
                                echo '<h5>'.get_sub_field('topic').'</h5>';
                                echo '<span class="meta" style="display:block;font-size:.9em;margin-bottom:10px;"><span class="date">'.get_sub_field('date').'</span> / '.get_sub_field('location').'</span>';
                                echo '<p>'.get_sub_field('description').' ';
                                if (get_sub_field('reg-link') != '') {
                                    echo '<a href="'.get_sub_field('reg-link').'" target="_blank">Register Now</a></p>';
                                } else {
                                    echo '</p>';
                                }

                            }
                        }
                      ?>
                  </div>
              </div>
          </div>
      </div>

      <!-- modals -->
      <div id="download-brochure" class="reveal-modal" data-reveal>
          <h5>Download Small Giants Brochure</h5>
          <p>Please complete the form below to be immediately directed to a downloadable interactive PDF of our services and work.</p>
          <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <div id="mailing-list" class="reveal-modal" data-reveal>
          <h5>Join Our Mailing List</h5>
          <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- secondary -->
      <div id="secondary-cont" class="row">
          <div class="large-4 medium-4 columns">
              <div class="panel buzz">
              	<h5>Latest Buzz:</h5>
              	<?
                  query_posts('order=DESC&orderby=date&posts_per_page=4&cat=-9,-10');
                  if ( have_posts() ) {
                ?>
                  <ul>
                <?
                      while ( have_posts() ) { the_post();
                          $postTitle = substr($careerTitle, 0, 120);
                ?>
                        <li><a href="<?php the_permalink() ?>"><span class="title"><? echo get_the_title(); ?></span></a></li>
                <?    } ?>
                  </ul>
                <?
                  }
                  wp_reset_query();
                ?>
              </div>
          </div>
          <div class="large-8 medium-8 columns text-right">
              <div class="bug" style="position:absolute;top:0;left:0;">
                  <?
                      $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '283');
                      $parent = new WP_Query( $args );
                      if ($parent->have_posts()) {
                          while ( $parent->have_posts() ) {
                              $parent->the_post();
                              $bugFact = htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8'));
                          }
                      }
                      wp_reset_query();
                  ?>
                  <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/bug-3.png" onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 200});">
              </div>
              <div style="" class="medium-text-center">
              <?
                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services', 'posts_per_page' => '4');
                  $parent = new WP_Query( $args );
                  if ($parent->have_posts()) {
                      while ( $parent->have_posts() ) {
                          $parent->the_post();

                          if (get_the_title() == 'Graphic Design') {
                              $serviceTitle = 'Graphic<br />Design';
                          } else {
                              $serviceTitle = get_the_title();
                          }
              ?>

                          <div class="circle dropshadow-light <? echo get_field('temp_color', get_the_ID()); ?>"><a href="<? echo get_permalink(get_the_ID()); ?>"><span><? echo $serviceTitle; ?></span></a></div>
              <?
                      }
                  }
                  wp_reset_query();
              ?>
              </div>
              <div class="bug" style="position:absolute;bottom:0;right:0;">
                  <?
                      $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '288');
                      $parent = new WP_Query( $args );
                      if ($parent->have_posts()) {
                          while ( $parent->have_posts() ) {
                              $parent->the_post();
                              $bugFact = htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8'));
                          }
                      }
                      wp_reset_query();
                  ?>
                  <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/beetle.png"  onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 200});">
              </div>
          </div>
      </div>

<?php get_footer(); ?>
