<?php
  /* Template Name: Experience Template */
  get_header();
?>

  <!-- primary -->
  <div id="primary-cont" class="row">
      <div class="large-8 large-push-4 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel0">
                  <div class="row">
                      <div class="large-12 columns">
                          <?
                              $slideshowID = array();
                              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'page_id' => get_the_ID(), 'post_type' => 'page');
                              $parent = new WP_Query( $args );
                              if ($parent->have_posts()) {
                                  while ( $parent->have_posts() ) {
                                      $parent->the_post();
                                      $posts = get_field('projects_for_slideshow');

                                      if( $posts ) {
                                          foreach( $posts as $post) {
                                              setup_postdata($post);
                                              $slideshowID[] = $post;
                                          }
                                          wp_reset_postdata();
                                      }
                                  }
                              }
                              wp_reset_query();


                              foreach ($slideshowID as $key => $value) {
                                  $slideCounter = 0;
                                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'page_id' => $value, 'post_type' => 'projects');
                                  $parent = new WP_Query( $args );
                                  if ($parent->have_posts()) {
                                      while ( $parent->have_posts() ) {
                                          $parent->the_post();
                                          if (have_rows('project-slide')) {
                                              while (have_rows('project-slide')) { the_row();
                                                  if ($slideCounter < 1) {
                                                      $image = wp_get_attachment_image_src(get_sub_field('image'), 'office-page');
                                                      if ($image[0] != '') {
                                                        $centerImage[] = get_sub_field('image-center');
                                                        $imgArray[] = $image[0];
                                                        $slideCounter++;
                                                      }
                                                  }
                                              }
                                          }
                                      }
                                  }
                                  wp_reset_query();
                              }
                          ?>

                          <?
                            if(have_rows('page-sections')) {
                                while (have_rows('page-sections')) { the_row();
                                    if(get_row_layout() == 'section-image') {
                                        $imageArray = wp_get_attachment_image_src(get_sub_field('page-image', 27), 'office-page');
                                        $imgArray[] = $imageArray[0];
                                    }
                                }
                            }
                                $counter = 0;
                          ?>
                            <ul class="bxslider">
                          <?
                              foreach ($imgArray as $key => $value) {
                                  if ($centerImage[$counter]) {
                                      echo '<li><img src="'.$value.'" style="display:block;margin:0 auto;"></li>';
                                  } else {
                                      if ($value != '') {
                                          echo '<li><img src="'.$value.'"></li>';
                                      }
                                  }
                                  $counter++;
                              }
                          ?>
                            </ul>

                      </div>
                  </div>
              </div>

              <div class="content" id="panel1">
                  <div class="row">
                      <div class="large-5 columns">
                          <h4><? echo get_the_title(27); ?></h4>
                          <? echo get_field('page-content-lr', 27); ?>
                      </div>
                      <div class="large-7 columns">
                          <?
                            if(have_rows('page-sections', 27)) {
                                while (have_rows('page-sections', 27)) { the_row();
                                    if(get_row_layout() == 'section-image'):
                                        $imageArray = wp_get_attachment_image_src(get_sub_field('page-image', 27), 'services');
                                        ?>
                                        <img src="<? echo $imageArray[0]; ?>" style="border:4px solid #ccc;">
                                        <?

                                    endif;
                                }
                            }
                          ?>
                      </div>
                  </div>
              </div>

              <div class="content" id="panel2">
                  <div class="row">
                      <div class="large-12 columns">
                          <h4><? echo get_the_title(29); ?></h4>
                          <br />
                          <?
                            if( have_rows('speaking-engagements', 29) ) {
                                while ( have_rows('speaking-engagements', 29) ) { the_row();
                                    echo '<h5>'.get_sub_field('topic').'</h5>';
                                    echo '<span class="meta" style="display:block;font-size:.9em;margin-bottom:10px;"><span class="date">'.get_sub_field('date').'</span> / '.get_sub_field('location').'</span>';
                                    echo '<p>'.get_sub_field('description').' ';
                                    if (get_sub_field('reg-link') != '') {
                                        echo '<a href="'.get_sub_field('reg-link').'" target="_blank">Register Now</a></p>';
                                    } else {
                                        echo '</p>';
                                    }

                                }
                            }
                          ?>
                      </div>
                  </div>
              </div>

          </div>
      </div>
      <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <div class="bug" style="position:absolute;left:-10px;top:-60px;">
              <?
                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '290');
                  $parent = new WP_Query( $args );
                  if ($parent->have_posts()) {
                      while ( $parent->have_posts() ) {
                          $parent->the_post();
                          $bugFact = htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8'));
                      }
                  }
                  wp_reset_query();
              ?>
              <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/bug-5.png"  onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 150});">
          </div>
          <h5>Client Type:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="#">Select Client Type</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'client-types');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                                $splitURL = explode('/', get_permalink(get_the_ID()));
                                $currTitle = str_replace('-', '', strtolower(get_the_title()));
                                if ($splitURL[count($splitURL) - 2] == $currTitle) {
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                                } else {
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                                }

                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Services:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select Service</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                                $splitURL = explode('/', get_permalink(get_the_ID()));
                                $splitURL[count($splitURL) - 2];
                                if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                    ?>
                                <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                                } else {
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                                }
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Project Type:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select Project Type</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'project-types');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                                $splitURL = explode('/', get_permalink(get_the_ID()));
                                $splitURL[count($splitURL) - 2];
                                if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                    ?>
                                <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                                } else {
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                                }
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab>
                      <dd><a class="dropshadow-extra-light" href="#panel1"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-1.png"> Why Small Giants</a></dd>
                      <dd><a class="dropshadow-extra-light" href="#" onClick="window.location.href='<? echo esc_url( home_url( '/' ) ); ?>speaking-engagements/'"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Speaking Engagements</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <p>Please complete the form below to be immediately directed to a downloadable interactive PDF of our services and work.</p>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns">
          <div class="panel quote-team">
              <div id="quote-slider" class="cycle-slideshow"
                                data-cycle-fx="fade"
                                data-cycle-timeout="10000"
                                data-cycle-slides="> blockquote"
                                data-cycle-auto-height="container">
                   <?
                      $counter = 1;
                      $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'testimonials');
                      $parent = new WP_Query( $args );
                      if ($parent->have_posts()) {
                          while ( $parent->have_posts() ) {
                              $parent->the_post();

                          ?>
                              <blockquote>
                                  <? echo get_field('testimonial-content'); ?>
                                  <span class="name">- <? echo get_field('testimonial-name'); ?></span>
                                  <span class="company"><? echo get_field('testimonial-company'); ?></span>
                              </blockquote>
                          <?
                          }
                      }
                      wp_reset_query();
                   ?>
              </div>
          </div>
      </div>
      <div class="large-8 medium-8 columns text-right">
          <div class="bug" style="position:absolute;top:0;left:0;">
              <?
                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '291');
                  $parent = new WP_Query( $args );
                  if ($parent->have_posts()) {
                      while ( $parent->have_posts() ) {
                          $parent->the_post();
                          $bugFact = addslashes(htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8')));
                      }
                  }
                  wp_reset_query();
              ?>
              <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/bug-2.png" onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 185});">
          </div>
          <div style="">
          <?
              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services', 'posts_per_page' => '4');
              $parent = new WP_Query( $args );
              if ($parent->have_posts()) {
                  while ( $parent->have_posts() ) {
                      $parent->the_post();

                      if (get_the_title() == 'Graphic Design') {
                          $serviceTitle = 'Graphic<br />Design';
                      } else {
                          $serviceTitle = get_the_title();
                      }
          ?>

                      <div class="circle dropshadow-light <? echo get_field('temp_color', get_the_ID()); ?>"><a href="<? echo get_permalink(get_the_ID()); ?>"><span><? echo $serviceTitle; ?></span></a></div>
          <?
                  }
              }
              wp_reset_query();
          ?>
          </div>
      </div>
  </div>

<?php get_footer(); ?>
