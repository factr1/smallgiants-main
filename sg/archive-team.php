<?php
  /* Template Name: Team Page */
  get_header();
?>

      <!-- primary -->
      <div id="primary-cont" class="row">
          <div class="large-4 columns sidebar dropshadow">
              <div class="row">
                  <div class="small-11 small-centered columns">
                      <ul class="team">
                          <?
                              $counter = 1;
                              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'team');
                              $parent = new WP_Query( $args );
                              if ($parent->have_posts()) {
                                  while ( $parent->have_posts() ) {
                                      $parent->the_post();
                                      $image = get_field('thumbnail');
                                      ?>
                                          <? if (get_the_title(get_the_ID()) == 'Abbey Dinh') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -5px -5px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Charlie Crews') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -5px -5px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Brianna Nessler') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -2px 0px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Danielle Feroleto, CPSM') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -4px 0px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Mya Hetrick') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat 0px -4px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else {?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } ?>
                                      <?
                                  }
                              }
                              wp_reset_query();
                          ?>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="large-8 columns stage">
              <div class="tabs-content team-content">
                  <div class="content active" id="panel0">
                      <iframe src="https://player.vimeo.com/video/130109100" width="642" height="470" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                  </div>
              </div>
          </div>
      </div>

      <!-- modals -->
      <div id="download-brochure" class="reveal-modal" data-reveal>
          <h5>Download Small Giants Brochure</h5>
          <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- secondary -->
      <div id="secondary-cont" class="row">
          <div class="large-4 medium-4 columns">
              <div class="panel quote-team">
                  <div id="quote-slider" class="cycle-slideshow"
                                    data-cycle-fx="fade"
                                    data-cycle-timeout="6000"
                                    data-cycle-slides="> blockquote"
                                    data-cycle-auto-height="container">
                       <?
                          $counter = 1;
                          $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'testimonials');
                          $parent = new WP_Query( $args );
                          if ($parent->have_posts()) {
                              while ( $parent->have_posts() ) {
                                  $parent->the_post();

                              ?>
                                  <blockquote>
                                      <? echo get_field('testimonial-content'); ?>
                                      <span class="name">- <? echo get_field('testimonial-name'); ?></span>
                                      <span class="company"><? echo get_field('testimonial-company'); ?></span>
                                  </blockquote>
                              <?
                              }
                          }
                          wp_reset_query();
                       ?>
                  </div>
              </div>
          </div>
          <div class="large-7 medium-7 columns text-left">
              <div class="bug" style="position:absolute;top:0;left:0;">
                  <?
                      $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '291');
                      $parent = new WP_Query( $args );
                      if ($parent->have_posts()) {
                          while ( $parent->have_posts() ) {
                              $parent->the_post();
                              $bugFact = addslashes(htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8')));
                          }
                      }
                      wp_reset_query();
                  ?>
                  <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/bug-2.png" onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 185});">
              </div>
              <div style="margin-top:20px;" class="circle dropshadow-light red"><span>Our Team</span></div>
              <p style="font-size:13px;margin-top:25px;">Small Giants is not your ordinary marketing company. In today's real estate, design and construction market it is important to build relationships and networks that last. It takes commitment. It takes focus. It takes Small Giants. This is what we do; we help you to be great through thoughtful strategy, solid business development, cutting edge creative services for marketing collateral and excellent training to support your firm at all levels.</p>
          </div>
          <div class="large-1 medium-1 columns text-right">
              <?
                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '283');
                  $parent = new WP_Query( $args );
                  if ($parent->have_posts()) {
                      while ( $parent->have_posts() ) {
                          $parent->the_post();
                          $bugFact = addslashes(htmlspecialchars(html_entity_decode(strip_tags(get_field('bug-fact')), ENT_QUOTES, 'UTF-8')));
                      }
                  }
                  wp_reset_query();
              ?>
              <img src="<? bloginfo('template_url'); ?>/assets/img/bug-1.png" style="margin-top:25px;" onmouseover="nhpup.popup('<? echo $bugFact; ?>', {'class': 'bug-facts red-bg dropshadow-light', 'width': 185});">
          </div>
      </div>

<?php get_footer(); ?>
