<?php  get_header(); ?>
<!-- primary -->
<div id="primary-cont" class="row">
  <div class="large-4 columns sidebar dropshadow">
    <h4>Latest Buzz:</h4>
    <?php query_posts('order=DESC&orderby=date&posts_per_page=4&cat=-9,-10');
    if ( have_posts() ) { ?>
      <ul>
        <?php
        while ( have_posts() ) {
          the_post();
          $postTitle = substr($careerTitle, 0, 120);
        ?>
          <li><a href="<?php  the_permalink()  ?>"><span class="title"><?php echo get_the_title(); ?></span></a></li>
        <?php } ?>
      </ul>
    <?php }wp_reset_query(); ?>
    <br />
    <h5>Categories:</h5>
    <div class="row">
      <div class="large-12 columns">
        <?php  wp_dropdown_categories( 'show_option_none=Select category' ); ?>
        <script type="text/javascript">
          var dropdown = document.getElementById("cat");
          function onCatChange() {
            if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
              location.href = "<?php  echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
            }
          }
          dropdown.onchange = onCatChange;
        </script>
      </div>
    </div>
    <h5 style="display:block;margin-top:20px;">Subscribe by Email:</h5>
    <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');?>
  </div>
  <div class="large-8 columns stage">
    <div class="tabs-content">
      <div class="content active" id="panel0" style="padding:20px 25px;">
        <div class="row">
          <div class="large-12 columns">
            <?php
            $cur_cat_id = get_cat_id( single_cat_title("",false) );

            $f1_cat_name = get_cat_name($cur_cat_id);

            echo '<h4>'.$f1_cat_name.'</h4>';
            ?>
          </div>
        </div>
        <div class="row">
          <ul class="small-block-grid-1 medium-block-grid-2">
            <?php
            query_posts('order=DESC&orderby=date&posts_per_page=4&cat='.$cur_cat_id);

            if ( have_posts() ) {
              while ( have_posts() ) {
                the_post();
            ?>
              <li>
                <a href="<?php  the_permalink()  ?>">
                  <?php if(has_post_thumbnail()):
                    the_post_thumbnail('post-thumb');
                  else:?>
                    <div class="grid-image">
                      <img src="<?php bloginfo('template_url');?>/assets/img/placeholder.jpg" alt="">
                    </div>
                  <?php
                  endif;
                  ?>
                  <h4 class="title"><?php echo get_the_title(); ?></h4>
                  <span><?php the_time('F j, Y');?></span>
                </a>
              </li>
              <?php } ?>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="content" id="panel1">
        <div class="row">
          <div class="large-12 columns">
            <h4><?php echo get_the_title(29); ?></h4>
            <?php echo get_field('page-content-lr', 29); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- modals -->
<div id="download-brochure" class="reveal-modal" data-reveal>
  <h5>Download Small Giants Brochure</h5>
  <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
  <a class="close-reveal-modal">&#215;</a>
</div>
<div id="mailing-list" class="reveal-modal" data-reveal>
  <h5>Give Us a Buzz</h5>
  <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>
  <a class="close-reveal-modal">&#215;</a>
</div>
<!-- secondary -->
<div id="secondary-cont" class="row">
  <div class="large-4 medium-4 columns text-center">
    <img src="<?php bloginfo('template_url'); ?>/assets/img/tgg-icon.png">
  </div>
  <div class="large-8 medium-8 columns">
    <a href="#" data-reveal-id="mailing-list" class="small button dropshadow-extra-light">Give us a buzz</a>
    <h2 class="blue">WE'D LOVE TO HEAR FROM YOU!</h2>
  </div>
</div>

<?php  get_footer(); ?>
