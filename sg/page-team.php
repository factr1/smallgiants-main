<?php
  /* Template Name: Team Page */
  get_header();
?>

      <!-- primary -->
      <div id="primary-cont" class="row">
          <div class="large-8 large-push-4 columns stage">
              <div class="tabs-content team-content">
                  <div class="content active" id="panel0">
                      <iframe src="https://player.vimeo.com/video/130109100" width="642" height="470" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                  </div>
                  <div class="content" id="panel1">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Danielle Feroleto</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel2">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Elisabeth Harris</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel3">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Kimberly Mickelson</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel4">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Abbey Dinh</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel5">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Nicole Waits</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel6">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Charlie Crews</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel7">
                      <div class="row">
                          <div class="large-12 columns">
                              <h2 class="uppercase">Team</h2>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-4 columns">
                              <br />
                              <div class="circle dropshadow-light"></div>
                          </div>
                          <div class="large-8 columns">
                              <h3 class="uppercase helvetica">Brianna Nessler</h3>
                              <span class="title">El Presidente</span>
                              <span class="links"><a href="#">V-card</a> | <a href="#">LinkedIn</a> | <a href="#">Speaking Resume</a></span>
                              <p>With 15 years of experience in the commercial real estate industry in Arizona, Danielle is recognized for her strong networking practices, championing creative marketing strategies and effective training skills. In addition to a successful marketing and business development career, Danielle is highly involved in associations and organizations that are instrumental in shaping policy and practices in the real estate industry. For more than five years, Danielle has worked with industry associations to provide training for many of the industry's leaders, through such organizations as Arizona Builders' Alliance (ABA), Building Owners and Managers Association (BOMA), and the Alliance for Construction Excellence (ACE).</p>
                              <p>As an Associate Faculty member at Arizona State University, Danielle teaches undergraduate and graduate students entering the marketplace a variety of marketing and business development skills to be successful in their chosen field. She has taught at the Del E. Webb School of Construction at ASU for the past 5 years.</p>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <div class="large-4 large-pull-8 columns sidebar dropshadow">
              <div class="row">
                  <div class="small-11 small-centered columns">
                      <dl class="tabs team-nav vertical" data-tab>
                          <dd><a href="#panel1"><div class="circle-team"></div><span>Danielle Feroleto</span></a></dd>
                          <dd><a href="#panel2"><div class="circle-team"></div><span>Elisabeth Harris</span></a></dd>
                          <dd><a href="#panel3"><div class="circle-team"></div><span>Kimberly Mickelson</span></a></dd>
                          <dd><a href="#panel4"><div class="circle-team"></div><span>Abbey Dinh</span></a></dd>
                          <dd><a href="#panel5"><div class="circle-team"></div><span>Nicole Waits</span></a></dd>
                          <dd><a href="#panel6"><div class="circle-team"></div><span>Charlie Crews</span></a></dd>
                          <dd><a href="#panel7"><div class="circle-team"></div><span>Brianna Nessler</span></a></dd>
                      </dl>
                  </div>
              </div>
          </div>
      </div>

      <!-- modals -->
      <div id="download-brochure" class="reveal-modal" data-reveal>
          <h5>Download Small Giants Brochure</h5>
          <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- secondary -->
      <div id="secondary-cont" class="row">
          <div class="large-4 medium-4 columns">
              <div class="panel quote-team">

              </div>
          </div>
      </div>

<?php get_footer(); ?>
