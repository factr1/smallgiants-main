// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var nano = require('gulp-cssnano');
var autoprefixer = require('gulp-autoprefixer');
var zip = require('gulp-zip');

// Lint Task
gulp.task('lint', function() {
    return gulp.src([
    	'sg/assets/js/**/*.js',
    	'!sg/assets/js/global.js',
    	'!sg/assets/js/global.min.js'
    	])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('sg/assets/scss/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('sg/assets/css/'));
});

// Minimize CSS
gulp.task('minify-css', ['sass'], function() {
  	return gulp.src([
  		'sg/assets/css/*.css',
  		'!sg/assets/css/*.min.css'
  		])
	  	.pipe(rename({
		        suffix: '.min'
	        }))
	    .pipe(nano({
        discardComments: {removeAll: true},
        autoprefixer: false
      }))
	    .pipe(gulp.dest('sg/assets/css/'));
});

// Concatenate & Minify JS
gulp.task('scripts', ['lint'], function() {
    return gulp.src([
    	'sg/assets/js/vendor/jquery.js',
      'sg/assets/js/foundation.min.js',
      'sg/assets/js/vendor/jquery.bxslider.js',
      'sg/assets/js/vendor/jquery.cycle.all.js',
      'sg/assets/js/vendor/nhpup_1.1.js',
      'sg/assets/js/main.js'
    	])
        .pipe(concat('global.js'))
        .pipe(gulp.dest('sg/assets/js'))
        .pipe(rename('global.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('sg/assets/js'));
});

//Minimize Images
gulp.task('images', function() {
    return gulp.src('sg/assets/img/*.{jpg,png,gif}')
    .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
    .pipe(gulp.dest('sg/assets/img'));
});


// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch([
    	'src/assets/js/*.js',
    	'!src/assets/js/global.js',
    	'!src/assets/js/global.min.js'
    	], ['scripts']);
    gulp.watch('sg/assets/scss/**/*.scss', ['styles']);
    gulp.watch([
      'sg/assets/css/*.css',
      '!sg/assets/css/*.min.css'
      ], ['styles']);
    gulp.watch('sg/assets/img/*.{jpg,png,gif}', ['images']);
});

// Concatenate CSS files
gulp.task('combine-css', ['minify-css'], function(){
  return gulp.src([
    'sg/assets/css/foundation.min.css',
    'sg/assets/css/main.min.css',
    'sg/assets/css/fontello.min.css',
    'sg/assets/css/animation.min.css',
    'sg/assets/css/jquery.bxslider.min.css',
    'sg/assets/css/video-js.min.css'
  ])
  .pipe(concat('all.min.css'))
  .pipe(gulp.dest('sg/assets/css'));
});

// Styles Task - minify-css is the only task we call, because it is dependent upon sass running first.
gulp.task('styles', ['combine-css']);

// Default Task
gulp.task('default', ['styles', 'scripts', 'images', 'watch']);
