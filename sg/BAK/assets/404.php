<?php get_header(); ?>
      <!-- Content -->
      <div id="content" class="row" data-equalizer>

          <div class="large-12 columns content-left-panel" data-equalizer-watch>
              <div class="padding">
                  <h3>404 Error: Page not found</h3>
                  <span><strong>This might be because:<br>
                  &#8226; You have typed the web address incorrectly, or<br>
                  &#8226; the page you were looking for may have been moved, updated or deleted.</strong></span>

                  <blockquote>
                      <em>“Liqui aut facessitias el et magnim laut atusandist, comnimi, voluptiandam receati optatur remUdant laut renienient volentiore que alibus cuptatur, unt facerovid quatio ea simos di te modi optiis iusan.”</em>
                      <span class="name"><em>Employee Name</em></span>
                      <span class="company"><em>Arrington Watkins</em></span>
                  </blockquote>
              </div>
          </div>
      </div>

<?php get_footer(); ?>
