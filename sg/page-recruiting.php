<?php
  /* Template Name: Recruiting */
  get_header();
?>

  <!-- primary -->
  <div id="primary-cont" class="row">
      <div class="large-8 large-push-4 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel0">
                    <div class="video-wrapper">
                      <iframe src="https://player.vimeo.com/video/137401725" width="100%" height="470" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>
              </div>

              <div class="content" id="panel1">
                  <div class="row quick-facts">
                      <h4><? echo get_the_title(35); ?></h4>
                      <? echo get_field('page-content-lr', 35); ?>
                  </div>
              </div>

              <div class="content" id="panel2">
                  <div class="row">
                      <div class="large-12 columns">
                          <? echo do_shortcode( '[wp_rss_aggregator limit="8" links_before=\'<ul class="rss-aggregator">\' link_before=\'<li class="feed-item-link">\']' ); ?>
                      </div>
                  </div>
              </div>

          </div>
      </div>
      <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <h5>Job Postings:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="#">Search</option>
                    <?
                      query_posts('cat=9&order=DESC&orderby=date&posts_per_page=5');
                      if ( have_posts() ) {
                          while ( have_posts() ) { the_post();
                              $postTitle = substr($careerTitle, 0, 120);
                    ?>
                            <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?    }
                      }
                      wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Candidate Profiles:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Search</option>
                    <?
                      query_posts('cat=10&order=DESC&orderby=date&posts_per_page=5');
                      if ( have_posts() ) {
                          while ( have_posts() ) { the_post();
                              $postTitle = substr($careerTitle, 0, 120);
                    ?>
                            <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?    }
                      }
                      wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab>
                      <dd><a class="dropshadow-extra-light" href="#panel1">Quick Facts for employers</a></dd>
                      <dd><a class="dropshadow-extra-light" href="#panel2">Resources</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="submit-resume" href="#">Submit Resume</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#">Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- modals -->
  <div id="submit-resume" class="reveal-modal" data-reveal>
      <h5>Submit Resume</h5>
      <?php echo do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <br />
  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns">
          <div class="panel buzz">
          	<h5>Latest Buzz:</h5>
          	<?
              query_posts('order=DESC&orderby=date&posts_per_page=4&cat=-9,-10');
              if ( have_posts() ) {
            ?>
              <ul>
            <?
                  while ( have_posts() ) { the_post();
                      $postTitle = substr($careerTitle, 0, 120);
            ?>
                    <li><a href="<?php the_permalink() ?>"><span class="title"><? echo get_the_title(); ?></span></a></li>
            <?    } ?>
              </ul>
            <?
              }
              wp_reset_query();
            ?>
          </div>
      </div>
      <div class="large-8 medium-8 columns text-right">
          <div class="bug" style="position:absolute;top:0;left:0;">
              <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/bug-3.png">
            <?
                $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '283');
                $parent = new WP_Query( $args );
                if ($parent->have_posts()) {
                    while ( $parent->have_posts() ) {
                        $parent->the_post();
            ?>

            <?
                    }
                }
                wp_reset_query();
            ?>

          </div>
          <div style="">
          <?
              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services', 'posts_per_page' => '4');
              $parent = new WP_Query( $args );
              if ($parent->have_posts()) {
                  while ( $parent->have_posts() ) {
                      $parent->the_post();

                      if (get_the_title() == 'Graphic Design') {
                          $serviceTitle = 'Graphic<br />Design';
                      } else {
                          $serviceTitle = get_the_title();
                      }
          ?>

                      <div class="circle dropshadow-light <? echo get_field('temp_color', get_the_ID()); ?>"><a href="<? echo get_permalink(get_the_ID()); ?>"><span><? echo $serviceTitle; ?></span></a></div>
          <?
                  }
              }
              wp_reset_query();
          ?>
          </div>
          <div class="bug" style="position:absolute;bottom:0;right:0;">
              <img class="bug-1" src="<? bloginfo('template_url'); ?>/assets/img/beetle.png">
            <?
                $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'bug-facts', 'page_id' => '283');
                $parent = new WP_Query( $args );
                if ($parent->have_posts()) {
                    while ( $parent->have_posts() ) {
                        $parent->the_post();
            ?>

            <?
                    }
                }
                wp_reset_query();
            ?>

          </div>
      </div>
  </div>

<?php get_footer(); ?>
