// Main JS to hold random JS

videojs.options.flash.swf = "<?php bloginfo('template_url');?>/assets/js/video/video-js.swf";

// Foundation JS
$(document).foundation();

// project pager
$('.bxslider').bxSlider({
  infiniteLoop: true,
  controls: false
});

$('.bxslider-exp').bxSlider({
  auto: true,
  infiniteLoop: true,
  controls: true
});

$('.bxslider-clients').bxSlider({
  infiniteLoop: true,
  controls: true,
  pager:false
});

$('.bxslider-client-img').bxSlider({
  //auto: true,
  infiniteLoop: true,
  controls: false,
  pager:false
});
