<?php get_header(); ?>

      <!-- primary -->
      <div id="primary-cont" class="row">
          <div class="large-8 large-push-4 columns stage">
              <div class="tabs-content team-content">
                  <div class="content active" id="panel0" style="padding:20px 25px;">
                      <?
                          $counter = 0;
                          $args = array('orderby' => 'menu_order', 'post_type' => 'team', 'page_id' => get_the_ID());
                          $parent = new WP_Query( $args );
                          if ($parent->have_posts()) {
                              while ( $parent->have_posts() ) {
                                  $parent->the_post();

                                  if( have_rows('member-photos', get_the_ID()) ) {
                                      $loopCounter = true;
                                      while ($loopCounter && have_rows('member-photos', get_the_ID()) ) { the_row();
                                          $image = get_sub_field('photo-item');
                                          $loopCounter = false;
                                          $imageArray = wp_get_attachment_image_src($image);
                                          ?>
                                              <div class="row">
                                                  <div class="large-12 columns">
                                                      <h2 class="uppercase">Team</h2>
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="large-4 columns">
                                                      <br />
                                                      <div class="circle-team-photos dropshadow-light" style="background:url('<? echo $imageArray[0] ?>') center center no-repeat;"></div>
                                                  </div>
                                                  <div class="large-8 columns">
                                                      <h3 class="uppercase helvetica"><? echo get_the_title(get_the_ID()); ?></h3>
                                                      <span class="title"><? echo get_field('member-title'); ?></span>
                                                      <span class="links"><? if (get_field('member-vcard') != '') { ?><a href="<? echo get_field('member-vcard'); ?>">V-card</a> |<? } ?> <? if (get_field('member-linkedin') != '') { ?><a href="<? echo get_field('member-linkedin'); ?>" target="_blank">LinkedIn</a> <? } ?> <? if (get_field('member-speaking-resume') != '') { ?> | <a href="<? echo get_field('member-speaking-resume'); ?>" target="_blank">Speaking Resume</a><? } ?></span>
                                                      <p><? echo get_field('member-bio'); ?></p>
                                                  </div>
                                              </div>
                                          <?
                                      }
                                  }
                              }
                          }
                          wp_reset_query();
                      ?>
                  </div>
              </div>
          </div>
          <div class="large-4 large-pull-8 columns sidebar dropshadow">
              <div class="row">
                  <div class="small-11 small-centered columns">
                      <ul class="team">
                          <?
                              $counter = 1;
                              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'team');
                              $parent = new WP_Query( $args );
                              if ($parent->have_posts()) {
                                  while ( $parent->have_posts() ) {
                                      $parent->the_post();

                                      $image = get_field('thumbnail');
                                      ?>
                                          <? if (get_the_title(get_the_ID()) == 'Abbey Dinh') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -5px -5px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Charlie Crews') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -5px -5px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Brianna Nessler') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -2px 0px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Danielle Feroleto, CPSM') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat -4px 0px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else if (get_the_title(get_the_ID()) == 'Mya Hetrick') { ?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat 0px -4px;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } else {?>
                                          <li><a href="<? echo get_permalink(get_the_ID()); ?>"><div class="circle-team" style="background:url('<? echo $image; ?>') no-repeat;"></div><span><? echo get_the_title(get_the_ID()); ?></span></a></li>
                                          <? } ?>
                                      <?

                                  }
                              }
                              wp_reset_query();
                          ?>
                      </ul>
                  </div>
              </div>
          </div>
      </div>

      <!-- modals -->
      <div id="download-brochure" class="reveal-modal" data-reveal>
          <h5>Download Small Giants Brochure</h5>
          <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- secondary -->
      <div id="secondary-cont" class="row">
          <div class="large-3 medium-3 columns">
              <div class="panel quote-team-single" style="margin-top:15px;">
                  <div id="quote-slider">
                      <span>Favorite Quote:</span>
                      <?
                        $counter = 1;
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'team', 'page_id' => get_the_ID());
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();

                                if( have_rows('member-quote', get_the_ID()) ) {
                                    while ( have_rows('member-quote', get_the_ID()) ) { the_row();
                            ?>
                                <blockquote>
                                    <? echo get_sub_field('quote'); ?>
                                </blockquote>
                            <?
                                    }
                                }
                            }
                        }
                        wp_reset_query();
                     ?>
                  </div>
              </div>
          </div>
          <div class="large-9 medium-9 small-12 columns text-left">
              <div style="margin-top:20px;" class="circle-our-team dropshadow-light red"><span>Our Team</span></div>
              <?
                $args = array('orderby' => 'menu_order', 'post_type' => 'team', 'page_id' => get_the_ID());
                $parent = new WP_Query( $args );
                unset($imageArray);
                if ($parent->have_posts()) {
                    while ( $parent->have_posts() ) {
                        $parent->the_post();

                        if( have_rows('member-photos', get_the_ID()) ) {
                            while (have_rows('member-photos', get_the_ID()) ) { the_row();
                                $image = get_sub_field('photo-item');
                                $imageArray = wp_get_attachment_image_src($image);
                    ?>
                            <div class="circle-team-photos dropshadow-light" style="background:url('<? echo $imageArray[0] ?>') no-repeat;margin-top:20px;"></div>
                    <?
                            }
                        }
                    }
                }
                wp_reset_query();
             ?>
          </div>
      </div>

<?php get_footer(); ?>
