<?php get_header(); ?>
      <!-- Content -->
      <div id="content" class="row" data-equalizer>
          <div class="large-12 columns content-left-panel" data-equalizer-watch>
              <div class="padding">
                  <h4>Search: <?php echo get_search_query(); ?></h4>
                  <?php if ( is_search() ) : ?>
                    <?php if ( have_posts() ) : ?>
                      <? while ( have_posts() ) : the_post(); ?>
                      	<div class="entry-summary">
                      	  <? echo get_the_title(); ?>
                      		<? //echo $content = substr(get_the_content(), 0, 80); ?>

                      	</div><!-- .entry-summary -->
                      <? endwhile; ?>
                    <?else :

              					echo 'No results found. Please try searching for a different term.';

              				endif;
              			?>
                	<?php else : ?>
                	<div class="entry-content">
                		<?php
                			the_content();
                			wp_link_pages( array(
                				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', '' ) . '</span>',
                				'after'       => '</div>',
                				'link_before' => '<span>',
                				'link_after'  => '</span>',
                			) );
                		?>
                	</div><!-- .entry-content -->
                	<?php endif; ?>
              </div>
          </div>
      </div>

<?php get_footer(); ?>
