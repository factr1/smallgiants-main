<?php
/* Template Name: Single Speaking Engagement Template */
get_header();
?>

      <!-- primary -->
      <div id="primary-cont" class="row">
          <div class="large-8 large-push-4 columns stage">
              <div class="tabs-content team-content">
                  <div class="content active" id="panel0" style="padding:20px 25px;">
                      <?
                          if (get_the_ID() == 727) {
                            $speakerID = 151;
                          } else if (get_the_ID() == 729) {
                            $speakerID = 152;
                          } else if (get_the_ID() == 725) {
                            $speakerID = 156;
                          }

                          $counter = 0;
                          $args = array('orderby' => 'menu_order', 'post_type' => 'team', 'page_id' => $speakerID);
                          $parent = new WP_Query( $args );
                          if ($parent->have_posts()) {
                              while ( $parent->have_posts() ) {
                                  $parent->the_post();

                                  if( have_rows('member-photos', $speakerID) ) {
                                      $loopCounter = true;
                                      while ($loopCounter && have_rows('member-photos', $speakerID) ) { the_row();
                                          $image = get_sub_field('photo-item');
                                          $loopCounter = false;
                                          $imageArray = wp_get_attachment_image_src($image);
                                          ?>
                                              <div class="row">
                                                  <div class="large-12 columns">
                                                      <h2 class="uppercase">Team</h2>
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="large-4 columns">
                                                      <br />
                                                      <div class="circle-team-photos dropshadow-light" style="background:url('<? echo $imageArray[0] ?>') center center no-repeat;"></div>
                                                      <span class="title" style="margin-top:25px;margin-bottom:5px;display:inline-block;">Speaking Topics</span>

                                                      <?php
                                                        if(have_rows('speaking-topics', $speakerID)) {
                                                      ?>
                                                      <ul>
                                                      <?php
                                                            while ( have_rows('speaking-topics', $speakerID) ) {
                                                                the_row();
                                                                echo '<li style="font-size:.8em;list-style:circle;list-style-position:inside;color:#5a5a54;">'.get_sub_field('topics').'</li>';
                                                            }
                                                      ?>
                                                      </ul>
                                                      <?php
                                                        }
                                                      ?>
                                                  </div>
                                                  <div class="large-8 columns">
                                                      <h3 class="uppercase helvetica"><? echo get_the_title($speakerID); ?></h3>
                                                      <span class="title"><? echo get_field('member-title'); ?></span>
                                                      <span class="links"><? if (get_field('member-vcard') != '') { ?><a href="<? echo get_field('member-vcard'); ?>">V-card</a> |<? } ?> <? if (get_field('member-linkedin') != '') { ?><a href="<? echo get_field('member-linkedin'); ?>" target="_blank">LinkedIn</a> <? } ?> <? if (get_field('member-speaking-resume') != '') { ?> | <a href="<? echo get_field('member-speaking-resume'); ?>" target="_blank">Speaking Resume</a><? } ?></span>
                                                      <p><? echo get_field('member-bio'); ?></p>
                                                  </div>
                                              </div>
                                          <?
                                      }
                                  }
                              }
                          }
                          wp_reset_query();
                      ?>
                  </div>
              </div>
          </div>
          <div class="large-4 large-pull-8 columns sidebar dropshadow">
              <h5>Speakers:</h5>
              <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                  <option value="#">Select a Speaker</option>
              <?
                  $currTitle = explode('/', strtolower(get_permalink( $post->ID )));
                  $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'team', 'post__in' => array(151, 152, 156));
                  $parent = new WP_Query( $args );
                  if ($parent->have_posts()) {
                      while ( $parent->have_posts() ) {
                          $parent->the_post();
                          if (get_the_ID() == 151) {
                            $speakerID = 727;
                          } else if (get_the_ID() == 152) {
                            $speakerID = 729;
                          } else if (get_the_ID() == 156) {
                            $speakerID = 725;
                          }

                          $splitURL = explode('/', get_permalink(get_the_ID()));
                          if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
              ?>
                          <option selected value="<? echo get_permalink($speakerID); ?>"><? echo get_the_title(); ?></option>
              <?
                          } else {
              ?>
                          <option value="<? echo get_permalink($speakerID); ?>"><? echo get_the_title(); ?></option>
              <?
                          }
                      }
                  }
                  wp_reset_query();
              ?>
              </select>
              <br />

              <h5>Topics Offered:</h5>
              <div class="row">
                  <div class="large-12 columns">
                      <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                          <option value="#">Select a Topic</option>
                          <?
                              $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'topics');
                              $parent = new WP_Query( $args );
                              if ($parent->have_posts()) {
                                  while ( $parent->have_posts() ) {
                                      $parent->the_post();
                                      $splitURL = explode('/', get_permalink(get_the_ID()));
                                      if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                          ?>
                                      <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                          <?
                                      } else {
                          ?>
                                      <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                          <?
                                      }
                                  }
                              }
                              wp_reset_query();
                          ?>
                      </select>
                  </div>
              </div>

              <div class="row">
                  <div class="small-11 small-centered columns">
                      <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                          <dd><a class="dropshadow-extra-light" href="#panel1"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-1.png"> Why Small Giants</a></dd>
                          <dd><a class="dropshadow-extra-light" data-reveal-id="book-speaker" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Book A Speaker/Training</a></dd>
                          <dd><a class="dropshadow-extra-light" data-reveal-id="mailing-list" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-3.png"> Join our mailing list</a></dd>
                          <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                      </dl>
                  </div>
              </div>
          </div>
      </div>

      <!-- modals -->
      <div id="download-brochure" class="reveal-modal" data-reveal>
          <h5>Download Small Giants Brochure</h5>
          <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <!-- secondary -->
      <div id="secondary-cont" class="row">
          <div class="large-4 medium-4 columns text-center">
              <img src="<? bloginfo('template_url'); ?>/assets/img/tgg-icon.png">
          </div>
          <div class="large-8 medium-8 columns">
              <a href="#" data-reveal-id="mailing-list" class="small button dropshadow-extra-light">Give us a buzz</a>
              <h2 class="blue">WE’D LOVE TO HEAR FROM YOU!</h2>
          </div>
      </div>

<?php get_footer(); ?>
