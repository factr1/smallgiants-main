<?php
  /* Template Name: Experience Template */
  get_header();
?>

  <!-- primary -->
  <div id="primary-cont" class="row">
      <div class="large-8 large-push-4 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel0">
                  <iframe width="642" height="465" src="http://www.youtube.com/embed/dvI5JuB6ThE" frameborder="0" allowfullscreen></iframe>
              </div>

              <div class="content" id="panel1">
                  <div class="row">
                      <div class="large-5 columns">
                          <h4><? echo get_the_title(27); ?></h4>
                          <? echo get_field('page-content-lr', 27); ?>
                      </div>
                      <div class="large-7 columns">
                          <?
                            // check if the flexible content field has rows of data
                            if( have_rows('page-sections') ):

                                 // loop through the rows of data
                                while ( have_rows('section-image') ) : the_row();

                                    if( get_row_layout() == 'page-image' ):

                                      echo '<img src="'.get_sub_field('page-image').'" width="385" height="272">';
                                    endif;

                                endwhile;

                            else :

                                // no layouts found

                            endif;

                          ?>
                      </div>
                  </div>
              </div>

              <div class="content" id="panel2">
                  <div class="row">
                      <div class="large-12 columns">
                          <h4>Speaking Engagements</h4>
                          <? echo get_field('page-content-lr', 29); ?>
                      </div>
                  </div>
              </div>

          </div>
      </div>
      <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <h5>Client Type:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="#">Select Client Type</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'client-types');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Services:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select Service</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Project Type:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'project-types');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab>
                      <dd><a class="dropshadow-extra-light" href="#panel1">Why Small Giants</a></dd>
                      <dd><a class="dropshadow-extra-light" href="#panel2">Speaking Engagements</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#">Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns">
          <div class="panel quote-team">
            <h5>Testimonial:</h5>
            <blockquote>
                Small Giants has done an amazing job setting up our website, business cards, letterhead and continually designing and sending out tombstones of our recent loan closings. They also handles all of our social media from setting up and monitoring all corporate accounts. We have used Small Giants for many years and they have a very creative team to help with any of your marketing or social media needs.
                <span class="name">- Jim Pierson</span>
                <span class="company">Founding Principal at Legacy Capital Advisors</span>
            </blockquote>
          </div>
      </div>
      <div class="large-8 medium-8 columns text-right">
          <div class="circle dropshadow-light red"><a href="marketing.html"><span>Marketing</span></a></div>
          <div class="circle dropshadow-light blue"><a href="training-coaching.html"><span>Training &amp;<br />Coaching</span></a></div>
          <div class="circle dropshadow-light green"><a href="biz-dev.html"><span>Business<br />Development</span></a></div>
          <div class="circle dropshadow-light purple"><a href="graphic-design.html"><span>Graphic<br />Design</span></a></div>
      </div>
  </div>

<?php get_footer(); ?>
