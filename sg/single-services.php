<?php get_header(); ?>

  <!-- primary -->
  <div id="primary-cont" class="row">
          <div class="large-8 large-push-4 columns stage">
              <div class="tabs-content">
                  <div class="content active" id="panel0">
                      <div class="row">
                          <div id="services" class="large-5 columns" style="padding:20px 25px;">
                              <? if(have_rows('service-type')) { ?>
                              <h5><? echo get_the_title(); ?></h5>
                              <ul class="services">
                              <?
                                    while (have_rows('service-type')) { the_row();
                                        echo '<li>'.get_sub_field('item').'</li>';
                                    }
                              ?>
                              </ul>
                              <? } ?>
                          </div>
                          <div class="large-7 columns" style="padding:20px 25px;">


                              <? if (get_the_title() == 'Graphic Design') { ?>
                                <iframe src="https://player.vimeo.com/video/130109779" width="325" height="225" style="border:4px solid #ccc;display:block;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                              <? } else if (get_the_title() == 'Business Development') {?>
                                <iframe src="https://player.vimeo.com/video/130109781" width="325" height="225" style="border:4px solid #ccc;display:block;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                              <? } else if (is_single(121)) {?>

                              <? $imageArray = wp_get_attachment_image_src(get_field('image', get_the_ID()), 'services'); ?>
                                <img src="<? echo $imageArray[0] ?>"  style="border:4px solid #ccc;">
                              <? } else if (get_the_title() == 'Marketing') {?>
                                <iframe src="https://player.vimeo.com/video/130109114" width="325" height="225" style="border:4px solid #ccc;display:block;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                              <? } else if (get_the_title() == 'Photo') { ?>
                                <? $imageArray = wp_get_attachment_image_src(get_field('image', get_the_ID()), 'services'); ?>
                                <img src="<? echo $imageArray[0] ?>"  style="border:4px solid #ccc;">
                              <? } ?>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel1">
                      <div class="row">
                          <div class="large-5 columns">
                              <h4><? echo get_the_title(27); ?></h4>
                              <? echo get_field('page-content-lr', 27); ?>
                          </div>
                          <div class="large-7 columns">
                              <?
                                if(have_rows('page-sections', 27)) {
                                    while (have_rows('page-sections', 27)) { the_row();
                                        if(get_row_layout() == 'section-image'):
                                            echo '<img src="'.get_sub_field('page-image', 27).'">';
                                        endif;
                                    }
                                }
                              ?>
                          </div>
                      </div>
                  </div>

                  <div class="content" id="panel2">
                      <div class="row">
                          <div class="large-12 columns">
                              <h4><? echo get_the_title(29); ?></h4>
                              <?
                                if( have_rows('speaking-engagements', 29) ) {
                                    while ( have_rows('speaking-engagements', 29) ) { the_row();
                                        echo '<strong>'.get_sub_field('date').'</strong> ';
                                        echo get_sub_field('topic');
                                        echo '<p>'.get_sub_field('description').'</p>';
                                        echo get_sub_field('location');
                                        echo get_sub_field('reg-link');
                                    }
                                }
                              ?>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
          <div class="large-4 large-pull-8 columns sidebar dropshadow">
              <h5>Client Type:</h5>
              <div class="row">
                  <div class="large-12 columns">
                      <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                        <option value="#">Select Client Type</option>
                        <?
                            $currTitle = explode('/', strtolower(get_permalink( $post->ID )));
                            $currTitle[count($currTitle) - 2];

                            $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'client-types');
                            $parent = new WP_Query( $args );
                            if ($parent->have_posts()) {
                                while ( $parent->have_posts() ) {
                                    $parent->the_post();
                                    $splitURL = explode('/', get_permalink(get_the_ID()));
                                    $splitURL[count($splitURL) - 2];
                                    if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                        ?>
                                    <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                        <?
                                    } else {
                        ?>
                                    <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                        <?
                                    }
                                }
                            }
                            wp_reset_query();
                        ?>
                      </select>
                  </div>
              </div>

              <h5>Services:</h5>
              <div class="row">
                  <div class="large-12 columns">
                      <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                        <option value="">Select Service</option>
                        <?
                            $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services');
                            $parent = new WP_Query( $args );
                            if ($parent->have_posts()) {
                                while ( $parent->have_posts() ) {
                                    $parent->the_post();
                                    $splitURL = explode('/', get_permalink(get_the_ID()));
                                    $splitURL[count($splitURL) - 2];
                                    if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                        ?>
                                    <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                        <?
                                    } else {
                        ?>
                                    <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                        <?
                                    }
                                }
                            }
                            wp_reset_query();
                        ?>
                      </select>
                  </div>
              </div>

              <h5>Project Type:</h5>
              <div class="row">
                  <div class="large-12 columns">
                      <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                        <option value="">Select Project Type</option>
                        <?
                            $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'project-types');
                            $parent = new WP_Query( $args );
                            if ($parent->have_posts()) {
                                while ( $parent->have_posts() ) {
                                    $parent->the_post();
                                    $splitURL = explode('/', get_permalink(get_the_ID()));
                                    $splitURL[count($splitURL) - 2];
                                    if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                        ?>
                                    <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                        <?
                                    } else {
                        ?>
                                    <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                        <?
                                    }
                                }
                            }
                            wp_reset_query();
                        ?>
                      </select>
                  </div>
              </div>

              <div class="row">
                      <div class="small-11 small-centered columns">
                          <dl class="tabs vertical" data-tab>
                              <dd><a class="dropshadow-extra-light" href="#panel1"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-1.png"> Why Small Giants</a></dd>
                              <dd><a class="dropshadow-extra-light" href="#" onClick="window.location.href='<? echo esc_url( home_url( '/' ) ); ?>speaking-engagements/'"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-2.png"> Speaking Engagements</a></dd>
                              <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#"><img src="<? bloginfo('template_url'); ?>/assets/img/icon-4.png"> Download Brochure</a></dd>
                          </dl>
                      </div>
                  </div>
              </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <p>Please complete the form below to be immediately directed to a downloadable interactive PDF of our services and work.</p>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>



  <!-- secondary -->
  <div id="secondary-cont" class="row">
      <div class="large-4 medium-4 columns">
          <div class="panel quote-team">
              <div id="quote-slider" class="cycle-slideshow"
                                data-cycle-fx="fade"
                                data-cycle-timeout="10000"
                                data-cycle-slides="> blockquote"
                                data-cycle-auto-height="container">
                   <?
                      $counter = 1;
                      $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'testimonials');
                      $parent = new WP_Query( $args );
                      if ($parent->have_posts()) {
                          while ( $parent->have_posts() ) {
                              $parent->the_post();

                          ?>
                              <blockquote>
                                  <? echo get_field('testimonial-content'); ?>
                                  <span class="name">- <? echo get_field('testimonial-name'); ?></span>
                                  <span class="company"><? echo get_field('testimonial-company'); ?></span>
                              </blockquote>
                          <?
                          }
                      }
                      wp_reset_query();
                   ?>
              </div>
          </div>
      </div>
      <div class="large-8 medium-8 columns">
      <?
          $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services', 'posts_per_page' => '4');
          $parent = new WP_Query( $args );
          if ($parent->have_posts()) {
              while ( $parent->have_posts() ) {
                  $parent->the_post();

                  if (get_the_title() == 'Graphic Design') {
                      $serviceTitle = 'Graphic<br />Design';
                  } else {
                      $serviceTitle = get_the_title();
                  }
      ?>

                  <div class="circle dropshadow-light <? echo get_field('temp_color', get_the_ID()); ?> text-center"><a href="<? echo get_permalink(get_the_ID()); ?>"><span><? echo $serviceTitle; ?></span></a></div>
      <?
              }
          }
          wp_reset_query();
      ?>
      </div>
  </div>

<?php get_footer(); ?>
