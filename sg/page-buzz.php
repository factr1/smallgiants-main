<?php
  /* Template Name: Buzz Template */
  get_header();
?>

  <!-- primary -->
  <div id="primary-cont" class="row">

      <div class="large-push-4 large-8 columns stage">
          <div class="tabs-content">
              <div class="content active" id="panel0" style="padding:20px 25px;">
                  <?
                    $args = array( 'numberposts' => '1', 'category' => '1,2,3,4,5,6' );
                  	$recent_posts = wp_get_recent_posts( $args );
                  	foreach( $recent_posts as $recent ){
                  ?>
                      <div class="row">
                          <div class="large-12 columns">
                              <h4 style="display:block;"><? echo $recent["post_title"] ?></h4>
                              <span class="meta"><span class="date"><? echo the_time('F d, Y'); ?></span> /
                              <?
                                $category = get_the_category($recent["ID"]);
                                if ($category[0]->cat_name != '') {
                                    echo 'in <a href="'.get_category_link( $category[0]->term_id ).'" title="'.$category[0]->cat_name.'">'.$category[0]->cat_name.'</a>';
                                }
                              ?>
                              </span>
                          </div>
                      </div>
                      <div class="row">
                          <div class="large-12 medium-12 columns">
                              <p><? echo get_field('blog-content', $recent["ID"]); ?></p>
                          </div>
                      </div>
                  <? } ?>
              </div>
              <div class="content" id="panel1">
                  <div class="row">
                      <div class="large-12 columns">
                          <h4><? echo get_the_title(29); ?></h4>
                          <? echo get_field('page-content-lr', 29); ?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <h4>Latest Buzz:</h4>
          <?
            query_posts('order=DESC&orderby=date&posts_per_page=4&cat=-9,-10');
            if ( have_posts() ) {
          ?>
            <ul>
          <?
                while ( have_posts() ) { the_post();
                    $postTitle = substr($careerTitle, 0, 120);
          ?>
                  <li><a href="<?php the_permalink() ?>"><span class="title"><? echo get_the_title(); ?></span></a></li>
          <?    } ?>
            </ul>
          <?
            }
            wp_reset_query();
          ?>
          <br />
          <h5>Categories:</h5>
          <div class="row">
              <div class="large-12 columns">


                    <?php wp_dropdown_categories( 'show_option_none=Select category' ); ?>
                  	<script type="text/javascript">
                  		<!--
                  		var dropdown = document.getElementById("cat");
                  		function onCatChange() {
                  			if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                  				location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                  			}
                  		}
                  		dropdown.onchange = onCatChange;
                  		-->
                  	</script>

              </div>
          </div>

          <h5 style="display:block;margin-top:20px;">Subscribe by Email:</h5>
          <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');?>
      </div>
  </div>

  <!-- modals -->
  <div id="download-brochure" class="reveal-modal" data-reveal>
      <h5>Download Small Giants Brochure</h5>
      <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
      <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="mailing-list" class="reveal-modal" data-reveal>
      <h5>Give Us a Buzz</h5>

      <?php echo do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]');?>

      <a class="close-reveal-modal">&#215;</a>
  </div>


<?php get_footer(); ?>
