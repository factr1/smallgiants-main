<?php get_header(); ?>

      <!-- primary -->
      <div id="primary-cont" class="row">
          <div class="large-8 large-push-4 columns stage">
              <div class="tabs-content">
                  <div class="content active" id="panel0"  style="padding:20px 25px;">
                      <?
                        if (have_posts()) {
                          while (have_posts()) { the_post();
                      ?>
                          <div class="row">
                              <div class="large-12 columns">
                                  <span class="meta"><span class="date"><? echo the_time('F d, Y'); ?></span> /
                                  <?
                                    $category = get_the_category($post->ID);
                                    if ($category[0]->cat_name != '') {
                                        echo 'in <a href="'.get_category_link( $category[0]->term_id ).'" title="'.$category[0]->cat_name.'">'.$category[0]->cat_name.'</a>';
                                    }
                                  ?>
                                  </span>
                              </div>
                          </div>
                          <div class="row">
                              <div class="large-12 medium-12 columns">
                                  <h4><? echo get_the_title(); ?></h4>

                                  <?php if(has_post_thumbnail()){
                                    the_post_thumbnail('post-featured');
                                  }?>

                                  <p><? echo get_field('blog-content'); ?></p>
                                  <? if (get_field('contact') != '') {?>
                                  <p>Name: <? echo get_field('contact'); ?><br />Phone: <? echo get_field('phone'); ?></p>
                                  <? if ($category[0]->cat_name == 'Candidate Profiles') { ?>
                                  <a target="_blank" href="mailto: <? echo get_field('email'); ?>?subject=<? echo get_field('subject'); ?>" class="button small dropshadow-extra-light" style="background-color:#f5831f;"><strong>I'm looking!</strong></a>
                                  <? } else { ?>
                                  <a target="_blank" href="mailto: <? echo get_field('email'); ?>?subject=<? echo get_field('subject'); ?>" class="button small dropshadow-extra-light" style="background-color:#f5831f;"><strong>I'm interested!</strong></a>
                                  <? } ?>
                                  <? } ?>
                              </div>
                          </div>
                      <? } ?>
                      <? } ?>
                  </div>
                  <div class="content" id="panel1">
                      <h4><? echo get_the_title(29); ?></h4>
                      <? echo get_field('page-content-lr', 29); ?>
                  </div>
              </div>
          </div>
          <div class="large-4 large-pull-8 columns sidebar dropshadow">
          <?
            $category = get_the_category($post->ID);
            if ($category[0]->cat_name == 'Jobs' || $category[0]->cat_name == 'Candidate Profiles') {
          ?>
          <h5>Job Postings:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="#">Search</option>
                    <?
                      $currTitle = explode('/', strtolower(get_permalink( $post->ID )));
                      $currTitle[count($currTitle) - 2];

                      query_posts('cat=9&order=DESC&orderby=date&posts_per_page=5');
                      if ( have_posts() ) {
                          while ( have_posts() ) { the_post();
                              $postTitle = substr($careerTitle, 0, 120);
                              $splitURL = explode('/', get_permalink(get_the_ID()));

                              if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                    ?>
                              <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                              } else {
                    ?>
                              <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                              }
                          }
                      }
                      wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Candidate Profiles:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Search</option>
                    <?
                      query_posts('cat=10&order=DESC&orderby=date&posts_per_page=5');
                      if ( have_posts() ) {
                          while ( have_posts() ) { the_post();
                              $postTitle = substr($careerTitle, 0, 120);
                              $splitURL = explode('/', get_permalink(get_the_ID()));

                              if ($splitURL[count($splitURL) - 2] == $currTitle[count($currTitle) - 2]) {
                    ?>
                              <option selected value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                              } else {
                    ?>
                              <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                              }
                          }
                      }
                      wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <div class="row">
              <div class="small-11 small-centered columns">
                  <dl class="tabs vertical" data-tab>
                      <dd><a class="dropshadow-extra-light" href="#panel1">Quick Facts for employers</a></dd>
                      <dd><a class="dropshadow-extra-light" href="#panel2">Resources</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="submit-resume" href="#">Submit Resume</a></dd>
                      <dd><a class="dropshadow-extra-light" data-reveal-id="download-brochure" href="#">Download Brochure</a></dd>
                  </dl>
              </div>
          </div>
      <? } else { ?>

          <h4>Latest Buzz:</h4>
          <?
            query_posts('order=DESC&orderby=date&posts_per_page=4&cat=-9,-10');
            if ( have_posts() ) {
          ?>
            <ul>
          <?
                while ( have_posts() ) { the_post();
                    $postTitle = substr($careerTitle, 0, 120);
          ?>
                  <li><a href="<?php the_permalink() ?>"><span class="title"><? echo get_the_title(); ?></span></a></li>
          <?    } ?>
            </ul>
          <?
            }
            wp_reset_query();
          ?>
          <br />
          <h5>Categories:</h5>
          <div class="row">
              <div class="large-12 columns">
                  <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                    <option value="">Select Category</option>
                    <?
                        $args = array('orderby' => 'menu_order', 'order' => 'ASC', 'post_type' => 'services');
                        $parent = new WP_Query( $args );
                        if ($parent->have_posts()) {
                            while ( $parent->have_posts() ) {
                                $parent->the_post();
                    ?>
                                <option value="<? echo get_permalink(get_the_ID()); ?>"><? echo get_the_title(); ?></option>
                    <?
                            }
                        }
                        wp_reset_query();
                    ?>
                  </select>
              </div>
          </div>

          <h5>Subscribe by Email:</h5>
          <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');?>
      <? } ?>
      </div>
      </div>

      <!-- modals -->
      <div id="download-brochure" class="reveal-modal" data-reveal>
          <h5>Download Small Giants Brochure</h5>
          <?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>

      <div id="mailing-list" class="reveal-modal" data-reveal>
          <h5>Join Our Mailing List</h5>
          <?php echo do_shortcode('[gravityform id="4" title="false" description="false" ajax="true"]');?>
          <a class="close-reveal-modal">&#215;</a>
      </div>


<?php get_footer(); ?>
